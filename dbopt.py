# -*- coding:UTF-8 -*-

"""
这个文件用于数据库操作，连接数据库，增删改查
"""


import pymongo
import block
import DataChain
import miner

class dbopt():
    def __init__(self):
        self.conn = pymongo.MongoClient('localhost',27017)
        self.db = self.conn.chain

    def insert_data(self,chain):
        self.db.data.insert(chain)

    def findall_data(self):
        re = []
        for x in self.db.data.find():
            re.append(x)
        return re

    def insert_block_to_chain(self,bloc):
        bl = block.block()
        if(bl.chain_check()):
            pass

if __name__ == '__main__':
    db = dbopt()
    data = db.findall_data()
    m = miner.miner()
    chain = DataChain.DataChain(data)
    print(chain)
    data_re = []
    for x in chain.Chain:
        data_re.append(x.get_data())
    for y in data_re:
        print(m.cal_data(y))
    print(chain.Chain[0].get_set())
    chain.Chain[0].nouce = m.cal_data(y)
