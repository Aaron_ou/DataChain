# -*- coding:UTF-8 -*-
"""
DataChain 的主模块
"""

import hashlib
import json
import block

class DataChain():
    def __init__(self,chain):
        self.Chain = []
        for x in chain:
            self.Chain.append(block.block(x))
