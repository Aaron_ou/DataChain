# -*- coding:UTF-8 -*-
"""
基础的 block类
block = {
data:data,
record:{
puser:puser
user:user
}
pHash:pHash
nouce:nouce
timetap:timetap
}
"""

import hashlib

class block():
    def __init__(self,block):
        self.data = block['data']
        self.record = block['record']
        self.pHash = block['pHash']
        self.nouce = block['nouce']
        self.index = block['index']
        self.timetap = block['timetap']

    def chain_check(self):
        if(self.data != b''):
            hash_check = hashlib.sha256()
            hash_check.update((str(self.data)+str(self.record)+str(self.pHash)+str(self.timetap)+str(self.nouce)).encode("utf-8"))
            re = hash_check.hexdigest()
            if(re[-3:] == '000'):
                return 0
            else:
                return 1
        else:
            return 0

    def get_set(self):
        block = {
            'data':self.data,
            'record':self.record,
            'pHash':self.pHash,
            'nouce':self.nouce,
            'index':self.index,
            'timetap':self.timetap
        }
        return block

    def get_hash(self):
        hash_check = hashlib.sha256()
        hash_check.update((str(self.data) + str(self.record) + str(self.pHash) + str(self.timetap) + str(self.nouce)).encode("utf-8"))
        re = hash_check.hexdigest()
        return re

    def get_data(self):
        hash_check = hashlib.sha256()
        hash_check.update((str(self.data) + str(self.record) + str(self.timetap)).encode("utf-8"))
        re = hash_check.hexdigest()
        return re

    def change_record(self,record):
        if(record['puser'] == self.record['user']):
            self.record['puser'] = self.record['user']
            self.record['user'] = record['user']
            return 1
        else:
            return 0