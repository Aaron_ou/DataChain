# -*- coding:UTF-8 -*-
"""
矿工专用
"""

from urllib import request
import hashlib
import json

class miner():
    def __init__(self):
        pass

    def get_data(self,address):
        re = request.Request(address)
        with request.urlopen(re) as f:
            data_json = f.read()
        data = json.loads(data_json.decode())
        return data

    def cal_data(self,data):
        re = 0
        i = 1
        while(re == 0):
            cal = hashlib.sha256()
            cal.update((data+str(i)).encode("utf-8"))
            res = cal.hexdigest()
            if (res[-3:] == '000'):
                re = 1
            else:
                i = i + 1
        print(res)
        return i
    def respone(self):
        pass

if __name__ == '__main__':
    m = miner()
    y = []
    data = m.get_data("http://localhost:8080/get_data")
    print(data[0])
    for x in data:
        y.append(m.cal_data(x))
    print(y)


